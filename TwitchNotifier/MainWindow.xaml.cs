﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;
using NLog;
using PostSharp;
using TwitchCSharp.Clients;
using PostSharp.Patterns.Diagnostics;
using PostSharp.Extensibility;
using TwitchCSharp.Models;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;

namespace TwitchNotifier
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>

    [LogException(AttributeTargetMemberAttributes = MulticastAttributes.Private | MulticastAttributes.Protected | MulticastAttributes.Internal | MulticastAttributes.Public)]
    public partial class MainWindow : Window
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ChannelManager manager = new ChannelManager();

        private readonly NotifyIcon notifyIcon = new NotifyIcon();

        public MainWindow()
        {
            InitializeComponent();

            notifyIcon.Icon = System.Drawing.Icon.FromHandle(Properties.Resources.NotifyIcon.Handle);
            notifyIcon.BalloonTipTitle = "TwitchNotifier is in the bushes...";
            notifyIcon.BalloonTipText = "Waiting for a steam to go live.";
            notifyIcon.MouseDoubleClick += NotifyIconOnMouseDoubleClick;


            manager.Live += ManagerOnLive;
            manager.Start();
        }

        private void ManagerOnLive(Channel channel, bool isLive)
        {
            if (isLive)
                NotificationManager.Show($@"{channel.DisplayName} is online", $@"Click on the notification to join the stream. {channel.Logo}");
            else
                NotificationManager.Show($@"{channel.DisplayName} is offline", string.Empty);
        }


        private async void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string channelName = ChannelAddress.Text;
            logger.Info("Add {0} channel's address", channelName);
            BindedStatus status = await manager.BindToChannel(channelName);
            switch (status)
            {
                case BindedStatus.ADDED:
                    NotificationManager.Show($@"{channelName} added", string.Empty);
                    break;
                case BindedStatus.ALREADY_ADDED:
                    NotificationManager.Show($@"{channelName} is already followed.", string.Empty);
                    break;
                case BindedStatus.DONT_EXIST:
                    NotificationManager.Show($@"{channelName} doesn't exist :/ ", string.Empty);
                    break;
                case BindedStatus.ERROR:
                    NotificationManager.Show("Error", $@"An error occured with Twitch API while searching {channelName} channel... :x");
                    break;
            }
        }

        private void NotifyIconOnMouseDoubleClick(object sender, MouseEventArgs mouseEventArgs)
        {
            this.Show();
            this.WindowState = WindowState.Normal;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(3000);
                this.Hide();
            }
            else if (this.WindowState == WindowState.Normal)
            {
                notifyIcon.Visible = false;
            }
        }
    }
}
