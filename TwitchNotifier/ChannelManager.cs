﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using NLog;
using PostSharp.Patterns.Diagnostics;
using PostSharp.Extensibility;
using TwitchCSharp.Clients;
using TwitchCSharp.Models;
using Timer = System.Timers.Timer;

namespace TwitchNotifier
{
    [LogException(AttributeTargetMemberAttributes = MulticastAttributes.Private | MulticastAttributes.Protected | MulticastAttributes.Internal | MulticastAttributes.Public)]
    public class ChannelIntervaled
    {
        public Channel Channel { get; set; }
        public bool IsLive { get; set; }
        public DateTime LastTimeChecked { get; set; }

        public ChannelIntervaled()
        {
            LastTimeChecked = DateTime.UtcNow;
        }

        public TimeSpan IntervalSinceLastCheck()
        {
            return DateTime.UtcNow - LastTimeChecked;
        }

        public void WasChecked()
        {
            LastTimeChecked = DateTime.UtcNow;
        }
    }

    public enum BindedStatus
    {
        ADDED,
        ALREADY_ADDED,
        DONT_EXIST,
        ERROR
    }

    [LogException(AttributeTargetMemberAttributes = MulticastAttributes.Private | MulticastAttributes.Protected | MulticastAttributes.Internal | MulticastAttributes.Public)]
    public class ChannelManager
    {
        public delegate void LiveEventHandler(Channel channel, bool isLive);
        public event LiveEventHandler Live;

        public static Logger logger = LogManager.GetCurrentClassLogger();

        protected TwitchReadOnlyClient twitchClient = default(TwitchReadOnlyClient);

        public List<ChannelIntervaled> channels = new List<ChannelIntervaled>();
        public Timer channelCheckTimer = new Timer();   

        public TimeSpan ChannelIntervalLiveCheck = TimeSpan.FromMinutes(1);

        public ChannelManager()
        {
            twitchClient = new TwitchReadOnlyClient();

            channelCheckTimer.AutoReset = false;
            channelCheckTimer.Interval = 5000;
            channelCheckTimer.Elapsed += ChannelCheckTimerOnElapsed;

            channelCheckTimer.Stop();
        }

        public void Start()
        {
            channelCheckTimer.Start();
        }

        public async Task<BindedStatus> BindToChannel(string channelName)
        {
            if (channels.Any(i => i.Channel.Name == channelName)) return BindedStatus.ALREADY_ADDED;

            Channel c = await Task.Run(() => twitchClient.GetChannel(channelName));
            if (c == null) return BindedStatus.DONT_EXIST;

            ChannelIntervaled intervaled = new ChannelIntervaled()
            {
                Channel = c,
                IsLive = await Task.Run(() => twitchClient.IsLive(c.Name))
            };
            channels.Add(intervaled);

            if(intervaled.IsLive) FireLive(c, true);
            return BindedStatus.ADDED;
        }

        public void Remove(string channelName)
        {
            ChannelIntervaled intervaled =
                channels.FirstOrDefault(channelIntervaled => string.Equals(channelIntervaled.Channel.Name, channelName));

            if (intervaled == default(ChannelIntervaled)) return;
            channels.Remove(intervaled);
        }

        protected void FireLive(Channel channel, bool isLive)
        {
            if (Live == null) throw new ArgumentNullException("No event are binded to Live Event.");
            Live(channel, isLive);
        }

        private void ChannelCheckTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            if (channels.Count <= 0) {
                channelCheckTimer.Start();
                return;
            }

            channelCheckTimer.Stop();
            List<ManualResetEvent> events = new List<ManualResetEvent>();

            try
            {
                int i = 0;
                foreach (var kvp in channels)
                {
                    // Time to check if live !
                    if (kvp.IntervalSinceLastCheck() > ChannelIntervalLiveCheck)
                    {
                        var e = new ManualResetEvent(false);
                        events.Add(e);
                        ThreadPool.QueueUserWorkItem(state =>
                        {
                            var evt = state as ManualResetEvent;
                            bool isLive = twitchClient.IsLive(kvp.Channel.Name);
                            // State is different since last time -> Notify
                            if (isLive != kvp.IsLive)
                            {
                                FireLive(kvp.Channel, isLive);
                                kvp.IsLive = isLive;
                            }
                            kvp.WasChecked();
                            evt.Set();
                        }, e);
                        i++;
                    }
                    
                }
            }
            finally
            {
                if(events.Count > 0)
                    WaitHandle.WaitAll(events.ToArray());

                channelCheckTimer.Start();
            }
        }
    }
}