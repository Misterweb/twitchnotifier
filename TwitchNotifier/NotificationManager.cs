﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Media.Animation;

namespace TwitchNotifier
{
    public enum NotificationDuration
    {
        INFINITE = 0,
        SHORT,
        LONG
    }

    public class Notification
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int DisplayTime { get; set; }
    }

    public static class NotificationManager
    {
        public static void Show(string title, string message, NotificationDuration duration = NotificationDuration.SHORT)
        {
            switch (duration)
            {
                case NotificationDuration.INFINITE:
                    Show(title, message, 0);
                    break;
                case NotificationDuration.SHORT:
                    Show(title, message, 5000);
                    break;
                case NotificationDuration.LONG:
                    Show(title, message, 15000);
                    break;
                default:
                    Show(title, message, 5000);
                    break;
            }
        }

        public static void Show(string title, string message, int milliseconds)
        {
            Thread thread = new Thread(o =>
            {
                Notification n = o as Notification;

                TimeSpan time = TimeSpan.FromMilliseconds(n.DisplayTime + 500);
                NotificationWindow notif = new NotificationWindow
                {
                    Title =
                    {
                        Text  = n.Title
                    },
                    Description = 
                    {
                        Text = n.Description
                    }
                };
                
                notif.CloseOpacityAnimation.KeyFrames[0] = new LinearDoubleKeyFrame(1, KeyTime.FromTimeSpan(time));
                notif.CloseOpacityAnimation.KeyFrames[1] = new LinearDoubleKeyFrame(0, KeyTime.FromTimeSpan(time + TimeSpan.FromMilliseconds(1000)));
                notif.ShowDialog();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start(new Notification()
            {
                Title = title,
                Description = message,
                DisplayTime = milliseconds
            });
        }
    }
}