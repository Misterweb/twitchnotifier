﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using PostSharp.Patterns.Diagnostics;
using PostSharp.Extensibility;

namespace TwitchNotifier
{
    /// <summary>
    /// Logique d'interaction pour NotificationWindow.xaml
    /// </summary>
    /// 
    [LogException(AttributeTargetMemberAttributes = MulticastAttributes.Private | MulticastAttributes.Protected | MulticastAttributes.Internal | MulticastAttributes.Public)]
    public partial class NotificationWindow : Window
    {
        public NotificationWindow()
        {
            InitializeComponent();

            Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                var screenWorkingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
                var trans = PresentationSource.FromVisual(this).CompositionTarget.TransformFromDevice;
                var corner = trans.Transform(new Point(screenWorkingArea.Right, screenWorkingArea.Bottom));

                this.Left = corner.X - this.ActualWidth + 1;
                this.Top = corner.Y - this.ActualHeight - 50;
            }));
        }

        private void AnimationOpacityCompleted(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
